Elogic Test Task!

Please create separate branch for your work.

This extension implements custom entity - Vendor.

Task list:

1. Create new product attribute allowing to select vendors(multiple) for each product.
2. On product page - display logo for all product vendors after product SKU(use default Luma theme)
3. On category page - display vendor names for each product after product name.

Additionally: Investigate existing code comparing to Magento best practices. Using separate doccument highlight all issues with current code base that you found. Add your recommendations and hints on improving code quality in all possible aspects.
