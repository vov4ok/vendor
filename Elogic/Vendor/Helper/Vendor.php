<?php


namespace Elogic\Vendor\Helper;

use Elogic\Vendor\Model\VendorFactory;
use Elogic\Vendor\Setup\InstallData;
use Magento\Catalog\Helper\Data;

class Vendor extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $vendorFactory;
    protected $eavConfig;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        VendorFactory $vendorFactory,
        $directivePatterns = []
    ) {
        $this->vendorFactory = $vendorFactory;
        parent::__construct($context);
    }

    public function getVendor($ids)
    {
        $result = [];
        $allVendors = $this->getAllVendorsArray();

        foreach (explode(',', $ids) as $vendorId) {
            !empty($fileName = $allVendors[$vendorId]["name"] ?? '') AND $result[] = $fileName;
        }

        return $result;

    }

    protected function getAllVendorsArray()
    {
        $vendorSetup = $this->vendorFactory->create();
        if (empty($vendors = $vendorSetup->getCollection()->getData())) {
            return [];
        } else {
            return array_column($vendors, null, 'id');
        }
        return $vendorSetup->getCollection()->getData() ?? [];
    }

}