<?php


namespace Elogic\Vendor\Block;

use Elogic\Vendor\Model\VendorFactory;
use Elogic\Vendor\Setup\InstallData;
use Magento\Backend\Block\Template\Context;
use Magento\Eav\Model\Config;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\StoreManagerInterface;

class Vendor extends Template
{

    protected $_registry;
    protected $eavConfig;
    protected $productRepository;
    protected $storeManager;
    protected $vendorFactory;

    public function __construct(
        Context $context,
        Registry $registry,
        Config $eavConfig,
        StoreManagerInterface $storeManager,
        VendorFactory $vendorFactory,
        array $data = []
    )
    {
        $this->eavConfig = $eavConfig;
        $this->_registry = $registry;
        $this->storeManager = $storeManager;
        $this->vendorFactory = $vendorFactory;
        parent::__construct($context, $data);
    }

    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    public function getImageUrlsArray()
    {

        $result = [];
        $product = $this->getCurrentProduct();
        $ids = $product->getData(InstallData::ATTRIBUTE_CODE);

        if (!empty(trim($ids))) {
            $allVendors = $this->getAllVendorsArray();

            $urlWithoutBaseName = $this->storeManager->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ) . 'vendor/logos/';

            foreach (explode(',', $ids) as $vendorId) {
                !empty($fileName = $allVendors[$vendorId]["logo"] ?? '') AND $result[] = $urlWithoutBaseName . $fileName;
            }
        }
        if (!empty($result)) {
            return $result;
        }


        return [];
    }

    protected function getAllVendorsArray()
    {
        $vendorSetup = $this->vendorFactory->create();
        if (empty($vendors = $vendorSetup->getCollection()->getData())) {
            return [];
        } else {
            return array_column($vendors, null, 'id');
        }
        return $vendorSetup->getCollection()->getData() ?? [];
    }
}